#######################################
#              Epitass                #
#             Makefile                #
#                                     #
#              Nocta                  #
#######################################

# Commandes principales
#  make clean
#  make all
#  make build
#  make run
#  make ass
#  make call_graph


# Variables
TREETOYUNDARAW=tree_toyunda_raw
PARSER=parser_toyunda_raw
LEXER=lexer_toyunda_raw
TREEV4PASS=tree_v4p_ass
TOYUNDATOASS=toyunda_raw_to_v4p_ass
LIBRAIRIES=
MAIN=epitass
PROGRAM=epitass
DOSSIERTOYUNDARAW=Toyunda_Raw
DOSSIERV4PASS=V4p_Ass
FORMATV4PASS=ass


# Target par défaut : compilation et nettoyage
all: build standalone clean

# Compilation
build : $(PROGRAM)
standalone: $(PROGRAM).standalone

%.mli: %.ml
	ocamlc -i $^ > $@

%.cmi: %.mli
	ocamlc -c $^

%.cmo: %.ml %.cmi
	ocamlc -c $<

$(PARSER).ml: $(PARSER).mly
	ocamlyacc -v $(PARSER).mly

$(LEXER).ml: $(LEXER).mll
	ocamllex $(LEXER).mll

$(PROGRAM): $(TREETOYUNDARAW).cmo $(PARSER).cmo $(LEXER).cmo $(TREEV4PASS).cmo $(TOYUNDATOASS).cmo $(MAIN).cmo
	ocamlc -o $(PROGRAM) $(LIBRAIRIES) $^

$(PROGRAM).standalone: $(TREETOYUNDARAW).cmo $(PARSER).cmo $(LEXER).cmo $(TREEV4PASS).cmo $(TOYUNDATOASS).cmo $(MAIN).cmo
	ocamlc -linkall -custom -o $(PROGRAM).standalone $(LIBRAIRIES) $^

# Dépendances
$(PARSER).ml: $(TREETOYUNDARAW).cmi
$(LEXER).cmo: $(PARSER).cmi
$(TOYUNDATOASS).cmo: $(TREEV4PASS).cmi
$(MAIN).cmo: $(LEXER).cmi $(TOYUNDATOASS).cmi


# Exécution
run : ass

# Test
test_mode:
	export OCAMLRUNPARAM='p'
prod_mode:
	export OCAMLRUNPARAM=''

# Génération des fichiers ass
ass : $(PROGRAM)
	for f in `ls $(DOSSIERTOYUNDARAW)`; do (./$(PROGRAM) $(DOSSIERTOYUNDARAW)/$$f > $(DOSSIERV4PASS)/$${f%%.*}.$(FORMATV4PASS)); done

# Nettoyage
clean :
	rm -f *.cmi *.cmo *.mli $(LEXER).ml $(PARSER).ml $(PARSER).output *~

clear : clean
	rm $(PROGRAM) $(DOSSIERV4PASS)/*.$(FORMATV4PASS)

clea : clean
