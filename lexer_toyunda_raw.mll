(************************\
 *       Epitass         *
 * lexer_toyunda_raw.mly *
 *                       *
 *  lexing  toyunda raw  *
 \************************)

(* Header section *)
{
  open Parser_toyunda_raw
  exception Invalid_char of char
  let print_string s = ()
  let print_char c = ()
  let print_newline () = ()
}

(* Definitions section *)
let integer = '-'?['0'-'9']+ (* Constante entière *)
let hexadig = ['0'-'9' 'A'-'Z']
let hexanum = hexadig hexadig
let colorField = '$' hexanum hexanum hexanum (hexanum)?
let ddrInstruction = ['<''>''^''v']*
let not_special_character_string = [^'\n''\r''<''>''^''v''0'-'9''{''}''|'':'','' ']+

(* Rules section *)

rule entrypoint = parse
  | "{0}{0}{needed at beginning of file for file format recognition by mplayer}" { entrypoint lexbuf } (*this f**** line does not respect the grammar documentation ! è_é*)
  | '\n'|"\r\n" { RETURN }
  | '{' { OPEN_BRACE }
  | integer as integer_string { NUM(integer_string) }
  | '}' { CLOSE_BRACE }
  | ' ' { SPACE }
  | "C:" { COLOR_SUBTITLE_OPTION }
  | colorField as color_string { COLOR_FIELD(color_string) }
  | ':' { COLON }
  | "{S:" { SIZE_SUBTITLE_OPTION } (*lexing "{S:" instead of "S" to make the parser work*)
  | "{O:" { POSITION_SUBTITLE_OPTION }
  | ',' { COMA }
  | "{B:" { BITMAP_SUBTITLE_OPTION }
  | "{D:" { DDR_SUBTITLE_OPTION }
  | ddrInstruction as ddr_string { DDR_INSTRUCTION(ddr_string) }
  | (' ')*'|' { PIPE }
  | "{c:" { COLOR_OPTION }
  | "{s:" { SIZE_OPTION }
  | "{o:" { POSITION_OPTION }
  | "{b:" { BITMAP_OPTION }
  | "{d:" { DDR_OPTION }
  | eof { EOF }
(*| ['\128'-'\254']* as nscs { let _ = Printf.eprintf "[Warning] Latin-1 / Extended ASCII caracter encountered : \"%s\"\n" (String.escaped nscs) in NSCS(nscs) }*)
  | not_special_character_string as nscs { (*let _ = Printf.eprintf "[test] lexer : \"%s\"\n" (String.escaped nscs) in*) NSCS(nscs) } (*See "garbage" part of parser*)
  | _ as invalid_char { raise (Invalid_char invalid_char) }

(* Trailer section *)
{}
